<?php
/**
 * The template for displaying archive news pages
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 */

get_header(); ?>

    <main class="main">

        <?php get_template_part('parts/breadcrumbs'); ?>

        <section class="projects projects-all">
            <div class="row">
                <?php if ($projects_title = get_field('news_section_title','options')): ?>
                    <div class="medium-6 columns text-left">
                        <h1 class="projects__title"><?php echo $projects_title; ?></h1>
                    </div>
                <?php endif; ?>

            </div>
            <div class="row">
                <?php while ( have_posts() ) : the_post();
                        $project_link = get_permalink(get_the_ID());
                        $item_thumbnail = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
                        $project_item_title = get_the_title(get_the_ID());
                        $event_categories = get_the_terms( $post, 'project_categories' );
                        ?>
                        <div class="large-4 medium-6 small-12 columns project-col1 matchHeight">
                            <a href="<?php echo $project_link ?>" class="projects__image-holder">
                                <div class="projects__image" style="background-image: url('<?php echo $item_thumbnail; ?>')"></div>
                            </a>
                            <p class="news-date"><?php $post_date = get_the_date( 'd-m-Y' ); echo $post_date; ?></p>
                            <a class="projects__title-single" href="<?php echo $project_link ?>"><?php echo $project_item_title ?></a>
                            <p><?php echo the_excerpt(); ?></p>
                        </div>
                <?php endwhile; ?>
            </div>
            <div class="row">
                <?php get_template_part('parts/pagination'); ?>
            </div>
        </section>
    </main>

<?php get_footer(); ?>