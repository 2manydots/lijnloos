<?php
/**
 * The template for displaying archive pages
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 */

get_header(); ?>

    <main class="main">

        <?php get_template_part('parts/breadcrumbs'); ?>

        <section class="projects projects-all">
            <div class="row">
                <?php if ($projects_title = get_field('projects_section_title','options')): ?>
                    <div class="medium-6 columns text-left">
                        <h1 class="projects__title"><?php echo $projects_title; ?></h1>
                    </div>
                <?php endif; ?>
                <div class="medium-6 columns text-right">
                    <div class="cat-filter">Bekijk:
                        <select class="cat-filter-options" id="cat-filter-options">
                            <option value="" disabled selected>Alle categorieen</option>
                            <option value="<?php echo get_post_type_archive_link('projecten');?>"><a href="<?php echo get_post_type_archive_link('projecten');?>">Alle categorieen</a></option>
                            <?php
                            $terms = get_terms( 'project_categories' );
                            foreach ( $terms as $term ) {
                                $term_link = get_term_link( $term );
                                if ( is_wp_error( $term_link ) ) {
                                    continue;
                                }
                                echo ' <option value="'.$term_link.'"><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></option>';
                            }
                            ?>
                        </select>
                    </div>                    
                </div>
            </div>
            <div class="row">

                <?php while ( have_posts() ) : the_post();
                    $project_link = get_permalink(get_the_ID());
                    $item_thumbnail = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
                    $project_item_title = get_the_title(get_the_ID());
                    $event_categories = get_the_terms( $post, 'project_categories' );
                    ?>
                    <div class="large-4 medium-6 small-12 columns project-col matchHeight">
                        <a href="<?php echo $project_link ?>" class="projects__image-holder">
                            <div class="projects__image" style="background-image: url('<?php echo $item_thumbnail; ?>')"></div>
                        </a>
                        <a class="projects__title-single" href="<?php echo $project_link ?>"><?php echo $project_item_title ?></a>

                        <p class="cat-name">
                            <?php  if ( $event_categories ):
                                foreach($event_categories as $event_cat):
                                    echo $event_cat->name;
                                endforeach;
                            endif; ?>
                        </p>
                    </div>
                <?php endwhile; ?>
            </div>
        </section>
    </main>


<?php get_footer(); ?>