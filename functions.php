<?php

/**
 * Add custom functions in the 'tmd/functions.php' file
 * or add extra files in the 'tmd' folder
 * to keep functionality structured.
 */

// Add JavaScript and CSS to wp_head
require_once("tmd/includes.php");

// Libraries
require_once("tmd/lib/phpQuery.php");

// Classes 
require_once("tmd/classes/sprite.class.php");
require_once("tmd/classes/retina-image.class.php");
require_once("tmd/classes/button.class.php");

// Functions
require_once("tmd/functions.php");

?>