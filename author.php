<?php
/**
 * The template for displaying Author archives
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */

get_header(); ?>

<main class="main">

    <?php get_template_part('parts/breadcrumbs'); ?>

    <div class="row">
        <div class="medium-12 columns">

            <?php if ( have_posts() ) : ?>

                <div class="row">
                    <div class="medium-12 columns">
                        <h1><?php _e( 'Published by', 'tmd-wp-grunt' ); ?> <?php echo get_the_author(); ?></h1>
                    </div>
                </div>

                <?php while ( have_posts() ) : the_post(); ?>

                    <div class="row">
                        <div class="medium-12 columns">
                            <h2><?php the_title(); ?></h2>
                            <p><?php custom_length_excerpt(54);  ?></p>
                                <a class="read-more" href="<?= get_permalink( get_the_ID() )?>" title="<?= get_the_title(); ?>"><?= __( 'lees meer', 'tmd-wp-grunt' ) ?></a>
                        </div>
                    </div>

                <?php endwhile;

                get_template_part('parts/pagination');

            else :

                get_template_part('parts/no-content');

            endif; ?>

        </div>
    </div>
</main>

<?php get_footer(); ?>