(function( $ ) {
    $(function() {
        function createCookie(name, value, days) {
            var expires;
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            }
            else expires = "";               

            document.cookie = name + "=" + value + expires + "; path=/";
        }
        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }
        function eraseCookie(name) {
            createCookie(name, "", -1);
        }
        
        // toggle menu icon
        $('.menu-icon').on('click', function () {
          $(this).addClass('opened');
        });
        $(document).ready(function () {
            $('.matchHeight').matchHeight();

        });

        $(".cat-filter-options").change(function(event){
            var term_id = $(".cat-filter-options").val();
            document.location.href = term_id;
        });
        $(window).resize(function () {
            $('.matchHeight').matchHeight();
        });
        $( ".menu-controls__open" ).click( "slow", function() {
            $(".mobile-nav").toggle();
            $(".mobile-menu-holder").slideToggle();
            $( ".menu-controls__open" ).fadeOut();
            $( ".menu-controls__close").css('display','inline-block');
            $('body').addClass('noScrolling');

            $('.sticky').addClass('fixed');
        });
        $( ".menu-controls__close" ).click( "slow", function() {
            $(".mobile-nav").toggle();
            $(".mobile-menu-holder").slideToggle();
            $( ".menu-controls__close" ).fadeOut();
            $( ".menu-controls__open").css('display','inline-block');
            $('body').removeClass('noScrolling');

            $('.sticky').removeClass('fixed');
        });
        //Add Check icon
        
        $( ".text-section .arrow-container svg" ).clone().prependTo( ".text-section ul > li" );
        $( ".image-text .arrow-container svg" ).clone().prependTo( ".image-text ul > li" );
        $( ".text-subtext__left .arrow-container svg" ).clone().prependTo( ".text-subtext .text-subtext__left ul > li" );
        $( ".text-subtext__right .check-icon-container svg" ).clone().prependTo( ".text-subtext .text-subtext__right ul > li" );
        $( ".titel-links .arrow-container svg" ).clone().prependTo( ".titel-links .titel-links__text ul > li" );
        
        $('.sticky-banner-container').each(function(){
            var $banner = $(this);
            
            if ( readCookie( 'banner-closed' ) == null ) {
                $banner.removeClass( 'closed' );
            }
            
            $banner.find('.close-btn').on( 'click', function(e){
                e.preventDefault();
                
                createCookie( 'banner-closed', 1, 30 );
                $banner.addClass( 'closed' );
            });
        });
    });
    
    $.fn.shuffle = function() {
 
        var allElems = this.get(),
            getRandom = function(max) {
                return Math.floor(Math.random() * max);
            },
            shuffled = $.map(allElems, function(){
                var random = getRandom(allElems.length),
                    randEl = $(allElems[random]).clone(true)[0];
                allElems.splice(random, 1);
                return randEl;
           });
 
        this.each(function(i){
            $(this).replaceWith($(shuffled[i]));
        });
 
        return $(shuffled);
 
    };
})(jQuery);

