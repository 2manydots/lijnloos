<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since Grunt Boilerplate 0.1.0
 * @author 2manydots
 */

get_header(); ?>

<main class="main">
    <?php if( $select_404_page = get_field('select_404_page', 'option') ): $post = $select_404_page; setup_postdata($post); ?>
        <?php
        if (have_rows('page_layouts')) {
            while (have_rows('page_layouts')) : the_row();
                if (get_row_layout() == 'image_text') {
                    get_template_part('parts/image-text');
                } elseif (get_row_layout() == 'quote_section') {
                    get_template_part('parts/quote-section');
                } elseif (get_row_layout() == 'text_block') {
                    get_template_part('parts/text-section');
                } elseif (get_row_layout() == 'image_block') {
                    get_template_part('parts/image-section');
                } elseif (get_row_layout() == 'video_block') {
                    get_template_part('parts/video-section');
                } elseif (get_row_layout() == 'projects_block') {
                    get_template_part('parts/projects-section');
                } elseif (get_row_layout() == 'click_through_block') {
                    get_template_part('parts/click-through-section');
                } elseif (get_row_layout() == 'text_with_links') {
                    get_template_part('parts/text-links-section');
                } elseif (get_row_layout() == 'hideshow_logos_section') {
                    get_template_part('parts/logos-section');
                } elseif (get_row_layout() == 'newsletter_section') {
                    get_template_part('parts/newsletter-section');
                } elseif (get_row_layout() == 'tekst_with_subtext') {
                    get_template_part('parts/text-subtext-section');
                } elseif (get_row_layout() == 'downloads_section') {
                    get_template_part('parts/downloads-section');
                } elseif (get_row_layout() == 'project_adviseur') {
                    get_template_part('parts/advertisement-section');
                } elseif (get_row_layout() == 'projectleden_section') {
                    get_template_part('parts/projectleden-section');
                }
            endwhile;
        };        
        ?>
    <?php wp_reset_postdata(); endif; ?>
    <?php get_template_part('parts/no-content'); ?>
</main>

<?php get_footer(); ?>
