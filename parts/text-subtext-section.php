<?php
/**
 * The partial template for displaying a "tekst met blokje section"
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<?php
$left_text = get_sub_field('left_text');
$right_text = get_sub_field('right_text');
?>
<section class="text-subtext">
    <div class="row">
        <div class="columns large-10 large-push-2">
            <div class="row">                
                <div class="medium-7 columns text-subtext__left">
                    <div class="arrow-container">
                        <?php new Sprite('arrow-right-icon'); ?>
                    </div>
                    <?php echo $left_text; ?>
                </div>
                <div class="medium-5 columns ">                    
                    <div class="text-subtext__right">
                        <div class="check-icon-container">
                            <?php new Sprite('check-icon'); ?>
                        </div>
                        <?php echo $right_text; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>