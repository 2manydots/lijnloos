<?php
/**
 * The partial template for displaying a quote section
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<?php
	$quote = get_sub_field('quote');
?>
<section class="quote-section">
<div class="row">
	<div class="large-10 large-push-2 small-12 columns quote-col">
		<div class="quote-section__wrapper">
		<blockquote class="quote text-left">
			<h2><?php echo $quote ; ?></h2>
		</blockquote>
		</div>
	</div>
</div>
</section>