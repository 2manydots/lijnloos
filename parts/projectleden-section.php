<?php
/**
 * The partial template for displaying a "advertisement section"
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<?php
$title = get_sub_field('title');

?>
<section class="projectleden">
    <div class="row">
        <div class="large-8 columns large-push-2">
            <h2 class="text-center projectleden__title"><?php echo $title;?></h2>
            <div class="projectleden__wrapper">
                <?php if (have_rows('members')): ?>
                    <?php while (have_rows('members')): the_row();
                        $image = get_sub_field('image');
                        $name = get_sub_field('name');
                        $job_title = get_sub_field('job_title');
                        $phone = get_sub_field('phone');
                        $email = get_sub_field('email');
                        $linked_in = get_sub_field('linked_in');
                        ?>
                        <div class="projectleden__item clearfix">
                                <?php $pl_photo = ($image) ? $image : get_template_directory_uri().'/images/default_prl.png'; ?>
                                <div class="projectleden__item-image" style="background-image: url('<?=$pl_photo?>');">

                                </div>
                                <div class="projectleden__item-content">
                                    <h5 class="projectleden__item-name"><?php echo $name; ?></h5>
                                    <p class="projectleden__item-job"><?php echo $job_title; ?></p>
                                    <?php if( $phone ): ?>
                                        <span class="projectleden__item-phone"><?php new Sprite('phone-icon'); ?><a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></span>
                                    <?php endif; ?>                                    
                                    <?php if( $email ): ?>
                                        <span class="projectleden__item-email"><?php new Sprite('email-icon'); ?><a href="mailto:<?php echo $email; ?>">Stuur een e-mail</a></span>
                                    <?php endif; ?>
                                    <?php if( $linked_in ): ?>
                                        <span class="projectleden__item-linkedin"><?php new Sprite('linkedin'); ?><a href="<?php echo $linked_in; ?>" target="_blank">LinkedIn</a></span>
                                    <?php endif; ?>
                                </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>