<?php
/**
 * The partial template for displaying a logboek section
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<?php if(have_rows('logboek')): ?>
	<section>
	<div class="row">
		<div class="large-9 large-centered small-12 columns logboeks-col">
			<?php while(have_rows('logboek')): the_row();
				$logboek_icon = get_sub_field('logboek_icon');
				$logboek_title = get_sub_field('logboek_title');
				$logboek_text = get_sub_field('logboek_text');
				?>
				<div class="row logboek-row">
					<div class="large-2 medium-2 small-12 columns"><img class="logboek-image" src="<?php echo $logboek_icon ?>" alt=""></div>
					<div class="large-10 medium-10 small-12 columns">
						<h3><?php echo $logboek_title ?></h3>
						<p class="logboek-text"><?php echo $logboek_text ?></p>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
	</section>
<?php endif; ?>