<?php
/**
 * The partial template for displaying a projects section
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>

<section class="projects click-throught-section">
    <div class="row">
        <?php if ($projects_title = get_sub_field('projects_title')): ?>
            <div class="columns text-center">
                <h2 class="projects__title"><?php echo $projects_title; ?></h2>
            </div>
        <?php endif; ?>
    </div>
    <div class="row text-center blocks-wrapper">
        <?php if (have_rows('items')): ?>
            <?php while (have_rows('items')): the_row();
                $choose_item = get_sub_field('choose_item');
                $project_link = get_permalink($choose_item);
                if ($item_thumbnail = get_sub_field('item_thumbnail')) {
                    $item_thumbnail = $item_thumbnail['sizes']['full_width_image'];
                } else {
                    $item_thumbnail = wp_get_attachment_url(get_post_thumbnail_id($choose_item));
                };
                if ($project_item_title = get_sub_field('item_title')) {
                    $project_item_title = get_sub_field('item_title');
                } else {
                    $project_item_title = get_the_title($choose_item);
                };
                ?>
                <div class="large-4 medium-6 small-12 columns project-col text-left">
                    <a href="<?php echo $project_link ?>" class="projects__image-holder">
                        <div class="projects__image" style="background-image: url('<?php echo $item_thumbnail; ?>')"></div>
                    </a>
                    <a class="projects__title-single" href="<?php echo $project_link ?>"><?php echo $project_item_title ?></a>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>
