<?php
    $projects_title = get_sub_field('projects_title');
    
    $category = get_sub_field( 'category' );
    $items = get_sub_field( 'items' );
    
    $query_args = [
        'post_type' => 'news',
        'posts_per_page' => 3
    ];
    
    if ( $items && ! empty( $items ) ) {
        $query_args['post__in'] = $items;
        $query_args['orderby'] = 'post__in';
        $query_args['posts_per_page'] = -1;
    } elseif ( $category ) {
        $query_args['tax_query'] = [
            [
                'taxonomy' => $category->taxonomy,
                'field'    => 'id',
                'terms'    => [ $category->term_id ],
                'operator' => 'IN'
            ]
        ];
    }
    
    $query = new WP_Query( $query_args );
?>
<section class="projects news click-throught-section">
    <div class="row">
        <?php if ( $projects_title ) : ?>
            <div class="columns text-center">
                <h2 class="projects__title"><?php echo $projects_title; ?></h2>
            </div>
        <?php endif; ?>
    </div>
    <div class="row text-center blocks-wrapper">
        <?php if ( $query->have_posts() ) : ?>
            <?php while ( $query->have_posts() ) : $query->the_post();
                    $item_thumbnail = get_the_post_thumbnail_url( get_the_ID(), 'large' );
                ?>
                <div class="large-4 medium-6 small-12 columns project-col text-left">
                    <a href="<?php the_permalink(); ?>" class="projects__image-holder">
                        <div class="projects__image" style="background-image: url('<?php echo $item_thumbnail; ?>')"></div>
                    </a>
                    <a class="projects__title-single" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </div>
            <?php endwhile; ?>
        <?php endif; wp_reset_query(); ?>
    </div>
</section>