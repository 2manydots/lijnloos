<?php
/**
 * The partial template for displaying a "advertisement section"
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<?php
$title = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
$phone = get_sub_field('phone');
$email = get_sub_field('email');
$image = get_sub_field('image');
?>
<section class="advertisement">
    <div class="row">
        <div class="small-12 columns">
            <div class="advertisement__wrapper">
                <div class="row">
                    <div class="large-7 medium-6 small-12 columns matchHeight large-push-1 text-left">
                        <h2 class="advertisement__title"><?php echo $title; ?></h2>
                        <h3 class="advertisement__subtitle"><?php echo $subtitle; ?></h3>
                        <span class="advertisement__phone"><?php new Sprite('phone-icon'); ?><?php echo $phone?></span>
                        <span class="advertisement__email"><?php new Sprite('email-icon'); ?><a href="mailto:<?php echo $email?>">Stuur mij een e-mail</a></span>
                    </div>
                    <div class="large-4 medium-6 small-12 columns text-right small-only-text-center matchHeight advertisement__image-holder">
                        <img src="<?php echo $image; ?>" alt="advertiser" class="advertisement__image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>