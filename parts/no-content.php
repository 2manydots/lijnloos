<?php
/**
 * The partial template for displaying a no content message
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>

<div class="no-content">
    <?php if (is_home() && current_user_can('publish_posts')) : ?>

        <div class="row">
            <div class="large-12 columns">
                <p><?php printf(__('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'tmd-wp-grunt'), admin_url('post-new.php')); ?></p>
            </div>
        </div>

    <?php elseif (is_search()) : ?>
        <div class="row">
            <div class="large-12 columns">
                <p><?php _e('Er zijn geen resultaten gevonden.', 'tmd-wp-grunt'); ?></p>

                <form role="search" method="get" id="searchform" class="searchform" action="<?php bloginfo('url') ?>">
                    <div>
                        <label class="screen-reader-text" for="s">Zoek naar:</label>
                        <input type="text" value="" name="s" id="s">
                        <button class="btn btn_round" id="searchsubmit">Zoeken</button>
                    </div>
                </form>
            </div>
        </div>
    <?php else : ?>
        <div class="row">
            <div class="large-12 columns">
                <!--<p><?php _e('De pagina die je zoekt, kan niet gevonden worden. Misschien helpt het om te zoeken binnen de site.', 'tmd-wp-grunt'); ?></p>-->

                <form role="search" method="get" id="searchform" class="searchform" action="<?php bloginfo('url') ?>">
                    <div>
                        <label class="screen-reader-text" for="s">Zoek naar:</label>
                        <input type="text" value="" name="s" id="s">
                        <!--<button class="btn btn_round" id="searchsubmit">Zoeken</button>-->
                        <button class="universal-button">Zoeken <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                    </div>
                </form>
            </div>
        </div>
    <?php endif; ?>
</div>
