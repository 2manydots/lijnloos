<?php
/**
 * The partial template for displaying a newsletter section
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<section class="newsletter-section">
                    <div class="row">
                        <div class="large-push-1 medium-push-1 large-10 medium-10 columns">
                    <div class="row">
                        <div class="large-8 medium-6 columns">
                            <?php if ($title = get_sub_field('title')): ?>
                                <h2 class="newsletter-section__title"><?php echo $title; ?></h2>
                            <?php endif; ?>
                            <?php if ($subtitle = get_sub_field('subtitle')): ?>
                                <h3 class="newsletter-section__subtitle"><?php echo $subtitle; ?></h3>
                            <?php endif; ?>
                        </div>
                        <div class="large-4 medium-6 columns newsletter-section__form-holder">
                            <?php if($external_form = get_sub_field('external_form')): ?>
                                <div class="ext-form">
                                    <?php echo $external_form; ?>
                                </div>
                            <?php elseif ($contact_form = get_sub_field('contact_form')): ?>
                                <?php echo do_shortcode('[gravityform id=' . $contact_form['id'] . ' title=false description=false ajax=true tabindex=49]'); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    </div>
</section>

