<?php
/**
 * The partial template for displaying a image section
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<?php if($page_info_image = get_sub_field('page_info_image')){?>
	<section class="image-section">
	<div class="row">
		<div class="large-10 large-push-2 medium-12 small-12 columns info-image-col">
			<?php
			$sizes = [
				[
					'size'  => 'full_width_image',
					'css'   => '1x'
				],
				[
					'size'  => 'full_width_image_2',
					'css'   => '2x'
				]
			];
			$eImage = new RetinaImage($page_info_image, $sizes);
			$eImage->setDimensions('large', 2);
			$eImage->printImage();
			?>
		</div>
	</div>
	</section>
<?php } ?>