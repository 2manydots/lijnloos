<?php
/**
 * The partial template for displaying breadcrumbs
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>


<div class="row">
    <div class="medium-12 columns">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
            <p>
            <?php if(function_exists('bcn_display'))
            {
                bcn_display();
            }?>
            </p>
        </div>
    </div>
</div>