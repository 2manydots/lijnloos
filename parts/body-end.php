<?php
    $body_end = get_field('body_end', 'option');
    $enable_scripts_fields = get_field('enable_scripts_fields', 'option');
    if($body_end && $enable_scripts_fields): ?>
    <?php echo $body_end; ?>
<?php endif; ?>