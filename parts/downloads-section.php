<?php
/**
 * The partial template for displaying a "downloads section"
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<?php
$title = get_sub_field('title');
?>
<section class="downloads">
    <div class="row">
        <div class="columns large-8 large-push-2">
            <div class="downloads__wrapper">
                <h5><?php echo $title; ?></h5>
                <?php if (have_rows('links')): ?>
                    <ul class="downloads__list">
                        <?php while (have_rows('links')): the_row(); ?>
                            <?php $select_src = get_sub_field('select_src'); ?>
                            <?php if( $select_src == 0 ): ?>
                                <?php 
                                    $link_text = get_sub_field('link_text');
                                    $file      = get_sub_field('file');
                                ?>
                                <?php if( $link_text && $file ): ?>
                                    <li class="file">
                                        <?php new Sprite('doc-icon-blue'); ?>
                                        <a class="downloads__links"
                                           href="<?php echo $file; ?>" target="_blank"><?php echo $link_text; ?></a>
                                    </li>
                                <?php endif; ?>
                            <?php elseif( $select_src == 1 ): ?>
                                <?php 
                                    $link_text = get_sub_field('link_text');
                                    $url      = get_sub_field('url');
                                ?>
                                <?php if( $link_text && $url ): ?>
                                    <li class="url">
                                        <?php new Sprite('globe-grid'); ?>
                                        <a class="downloads__links"
                                           href="<?php echo $url; ?>" target="_blank"><?php echo $link_text; ?></a>
                                    </li>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>