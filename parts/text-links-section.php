<?php
/**
 * The partial template for displaying a "tekst met titel links"
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<?php
$titel_title = get_sub_field('title');
$titel_text = get_sub_field('text');
?>
<section class="titel-links">
    <div class="arrow-container">
        <?php new Sprite('arrow-right-icon'); ?>
    </div>
    <div class="row page-content-row">
        <div class="medium-5 columns">
           <h1 class="titel-links__title"> <?php echo $titel_title; ?></h1>
        </div>
        <div class="medium-7 columns">
           <div class="titel-links__text"><?php echo $titel_text; ?></div>
        </div>
    </div>
</section>