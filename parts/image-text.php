<?php
/**
 * The partial template for displaying a text and image section
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<?php
	$image = get_sub_field('image');
	$align = get_sub_field('image_alignement');
	$title = get_sub_field('title');
	$text = get_sub_field('text');
	$button_link = get_sub_field('button_link');
	$button_text = get_sub_field('button_text');
?>
<section class="image-text">
        <div class="arrow-container">
            <?php new Sprite('arrow-right-icon'); ?>
        </div>
	<div class="row image-text-block-row">
		<div class="large-5 medium-6 small-12 columns<?php echo $align == 'right' ? ' large-push-7 medium-push-6' : '' ?> ">
				<img class="<?php echo $align == 'right' ? ' large-push-7 medium-push-6' : 'image-text__image-holder' ?>" src="<?php echo $image['sizes']['large']; ?>" alt="image">
		</div>
		<div class="large-6 medium-6 small-12 columns<?php echo $align == 'right' ? ' large-pull-5 medium-pull-6 image-text__text-holder-right' : ' large-push-1' ?> image-text__text-holder">
			<h2><?php echo $title ; ?></h2>
			<p><?php echo $text ; ?></p>
			<?php if($button_link): ?>                        
				<div class="button-container">
					<a href="<?php echo $button_link['url'] ?>" class="universal-button" <?php echo esc_attr($button_link['target']); ?>><?php echo $button_link['text'] ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>