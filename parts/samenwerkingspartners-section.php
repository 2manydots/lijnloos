<section class="samenwerkingspartners_section">
    <?php 
        $title = get_sub_field('title');
        $title_offset = get_sub_field('title_offset');
        //$items;
    ?>
    
        <?php if($title): ?>
            <div class="row">
                <div class="<?php echo ($title_offset ? 'large-8 large-push-2': 'large-12');?> medium-12 small-12 columns">
                    <h2 class="sec-title"><?php echo $title; ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <?php if( have_rows('items') ): ?>
            <div class="row">
                <div class="items-wrapper">
                    <?php while( have_rows('items') ): the_row(); ?>
                        <?php 
                            $title = get_sub_field('title');
                            $image = get_sub_field('image');
                            $url   = get_sub_field('url');
                        ?>
                        <?php if( $title && $image ): ?>
                            <div class="item-wrapper">
                                <div class="image-wrapper">
                                    <div class="table-wrapper">
                                        <div class="table-cell-wrapper">
                                            <div class="inner-image-wrapper">
                                                <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bottom-area">
                                    <h5 class="title"><?php echo $title; ?></h5>
                                    <?php if( $url ): ?>
                                        <a href="<?php echo $url; ?>" class="read-w" target="_blank">
                                            Bekijk de website
                                            <span class="arrow">
                                                <svg version="1.1" id="Layer_1_arrow_right_icon_grey" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                            viewBox="0 0 20 14" style="enable-background:new 0 0 20 14;" xml:space="preserve">
                                                   <style type="text/css">
                                                           .arrow_right_icon_grey0{fill:none;}
                                                           .arrow_right_icon_grey1{fill:#666666;}
                                                   </style>
                                                   <rect y="-2.9999812" class="arrow_right_icon_grey0" width="19.9999619" height="19.9999619"/>
                                                   <g>
                                                           <path class="arrow_right_icon_grey1" d="M19.7275982,6.3424072l-5.9516611-5.9516611c-0.3631248-0.3631258-0.9520416-0.3631258-1.3151674,0
                                                                   c-0.3632002,0.3632001-0.3632002,0.951968,0,1.3151684l4.36413,4.3641295H0.9299471C0.4163931,6.070044,0,6.4864373,0,6.9999909
                                                                   c0,0.5134792,0.4163931,0.9299474,0.9299471,0.9299474h15.8949528l-4.3639803,4.3641291
                                                                   c-0.3632002,0.3632011-0.3632002,0.9519691,0,1.3151684c0.1815262,0.1814508,0.4195919,0.2723627,0.6575842,0.2723627
                                                                   s0.4759836-0.0909119,0.6575851-0.2723627l5.9515114-5.9516611C20.0907993,7.2943754,20.0907993,6.7056069,19.7275982,6.3424072z"
                                                                   />
                                                   </g>
                                                </svg>
                                            </span>
                                        </a>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </div>                
            </div>            
        <?php endif; ?>
</section>