<?php
/**
 * The partial template for displaying a logo slider
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#slider').slick({
            cssEase: 'ease',
            arrows: true,
            dots: false,
            infinite: true,
            nextArrow: '.property-next',
            prevArrow: '.property-prev',
            speed: 500,
            slidesToShow: 6,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    });
</script>

<?php if ( get_field( 'hide_header_image_copy' ) ): ?>
    <div class="row">
        <div class="colunmns text-center">
            <h2 class="slider-title"><?php the_field('logo_title','options'); ?></h2>
        </div>
    </div>
<div class="row">
    <div class="colunmns">
        <div id="slider" class="logos-slider slick-slider">
            <?php if ($images = get_field('logos_gallery','options')): ?>
            <?php shuffle($images); ?>
                <?php foreach ($images as $image): ?>
                    <div class="slick-slide">
                        <div class="img-holder">
                             <img src="<?php echo $image['sizes']['full_width_image']; ?>" alt="logos">
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="columns">
        <div class="slider-controls text-center">
            <a class="property-prev" href="javascript:void(0)"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
            <?php if($showhide_overview_button = get_field('show_overview_button','options')): ?>
                <?php $overview_button_link= get_field('overview_button_link','options'); ?>
                <?php $overview_button_title= get_field('overview_button_link','options'); ?>
                <a class="overview" href="<?php echo $overview_button_link['url'];?>"><?php echo $overview_button_link['text']; ?></a>
            <?php endif; ?>
            <a class="property-next" href="javascript:void(0)"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
        </div>
    </div>
</div>

<?php else: // field_name returned false ?>

<?php endif; // end of if field_name logic ?>

