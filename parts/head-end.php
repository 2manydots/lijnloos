<?php
    $header_script = get_field('header_script', 'option');
    $enable_scripts_fields = get_field('enable_scripts_fields', 'option');
    if($header_script && $enable_scripts_fields): ?>
    <?php echo $header_script; ?>
<?php endif; ?>