<?php
/**
 * The partial template for displaying a projects section
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>

<section class="projects click-throught-section">
    <div class="row">
        <?php if ($projects_title = get_sub_field('projects_title')): ?>
            <div class="medium-8 columns text-left">
                <h2 class="projects__title"><?php echo $projects_title; ?></h2>
            </div>
        <?php endif; ?>
        <?php if ($internal_page_link = get_sub_field('internal_page_link')): ?>
            <div class="medium-4 columns text-right">
                <a href="<?php echo $internal_page_link['url']; ?>"
                   class="projects__internal-link"><?php echo $internal_page_link['text']; ?> <?php new Sprite('arrow-right-icon'); ?></a>
            </div>
        <?php endif; ?>
    </div>
    <div class="row text-center blocks-wrapper">
        <?php if (have_rows('items')): ?>
            <?php while (have_rows('items')): the_row();
                $item_title     = get_sub_field('item_title');
                $item_type      = get_sub_field('item_type');
                $url            = get_sub_field('url');
                $select_item    = get_sub_field('select_item');
                $item_thumbnail = get_sub_field('item_thumbnail');
                
                if( $item_type == 1 ) {
                    $item_url = $select_item;
                }
                elseif( $item_type == 2 ) {
                    $item_url = $url;
                }
//                $choose_item = get_sub_field('choose_item');
//                $project_link = get_permalink($choose_item);
//                if ($item_thumbnail = get_sub_field('item_thumbnail')) {
//                    $item_thumbnail = $item_thumbnail['sizes']['full_width_image'];
//                } else {
//                    $item_thumbnail = wp_get_attachment_url(get_post_thumbnail_id($choose_item));
//                };
//                if ($choose_item_text = $choose_item['text']) {
//                    $choose_item_text = $choose_item['text'];
//                } else {
//                    $choose_item_text = get_the_title($choose_item);
//                };
                ?>
                <div class="large-4 medium-6 small-12 columns project-col matchHeight">
                    <a href="<?php echo $item_url; ?>" class="projects__image-holder" <?php if( $item_type == 2 ): ?> target="_blank"<?php endif; ?>>
                        <div class="projects__image" style="background-image: url('<?php echo $item_thumbnail['sizes']['full_width_image']; ?>')"></div>
                    </a>
                    <a href="<?php echo $item_url; ?>" class="projects__title-single" <?php if( $item_type == 2 ): ?> target="_blank"<?php endif; ?>><?php echo $item_title; ?></a>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>
