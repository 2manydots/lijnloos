<?php
    $projects_title = get_sub_field( 'projects_title' );
    $post_type   = 'projecten';
    $category    = get_sub_field( 'category' );
    $items       = get_sub_field( 'items' );
    $button_text = get_sub_field( 'button_text' );

    $query_args = [
        'post_type'      => $post_type,
        'posts_per_page' => 6
    ];
    
    if ( $items && ! empty( $items ) ) {
        $query_args['post__in'] = $items;
        $query_args['orderby'] = 'post__in';
        $query_args['posts_per_page'] = -1;
    } elseif ( $category ) {
        $query_args['tax_query'] = [
            [
                'taxonomy' => $category->taxonomy,
                'field'    => 'id',
                'terms'    => [ $category->term_id ],
                'operator' => 'IN'
            ]
        ];
    }
    
    $query = new WP_Query( $query_args );
?>
<section class="projects new-projects click-throught-section">
    <div class="row">
        <?php if ( $projects_title ) : ?>
            <?php 
                $title_col_classes = 'medium-8 text-left';
                
                if ( ! $button_text ) :
                    $title_col_classes = 'medium-12 text-center';
                endif;
            ?>
            <div class="<?php echo $title_col_classes; ?> columns">
                <h2 class="projects__title"><?php echo $projects_title; ?></h2>
            </div>
        <?php endif; ?>
        <?php if ( $button_text ) : ?>
            <div class="medium-4 columns text-right">
                <a href="<?php echo get_post_type_archive_link( $post_type ); ?>"
                   class="projects__internal-link"><?php echo $button_text; ?> <?php new Sprite('arrow-right-icon'); ?></a>
            </div>
        <?php endif; ?>
    </div>
    <div class="row text-center blocks-wrapper">
        <?php if ( $query->have_posts() ) : ?>
            <?php while ( $query->have_posts() ) : $query->the_post();
                $item_thumbnail = get_the_post_thumbnail_url( get_the_ID(), 'large' );
                ?>
                <div class="large-4 medium-6 small-12 columns project-col">
                    <div class="project-item">
                        <a href="<?php the_permalink(); ?>" 
                            class="projects__image-holder">
                             <div class="projects__image" 
                                  style="background-image: url('<?php echo $item_thumbnail; ?>')"></div>
                        </a>
                        <div class="info-wrapper">
                            <a href="<?php the_permalink(); ?>" 
                                class="projects__title-single"><?php the_title(); ?></a>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; wp_reset_query(); ?>
    </div>
</section>