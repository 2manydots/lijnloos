<?php
/**
 * The partial template for displaying a video section
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<?php if($video = get_sub_field('video')){?>
	<section class="video-section">
	<div class="row">
		<div class="large-10 large-push-2 medium-12 small-12 columns info-image-col">
			<?php
			echo $video;
			?>
		</div>
	</div>
	</section>
<?php } ?>