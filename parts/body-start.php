<?php
    $body_start = get_field('body_start', 'option');
    $enable_scripts_fields = get_field('enable_scripts_fields', 'option');
    if($body_start && $enable_scripts_fields): ?>
    <?php echo $body_start; ?>
<?php endif; ?>