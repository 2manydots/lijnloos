<?php
/**
 * The partial template for displaying a color blocks
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<?php if (have_rows('block_builder')): ?>
    <div class="row block-builder-row">
        <?php $i = 1; ?>
        <?php while (have_rows('block_builder')): the_row();
            $block_color = get_sub_field('block_color');
            $title = get_sub_field('title');
            $text = get_sub_field('text');
            $link = get_sub_field('link');

            ?>
            <div
                class="large-6 medium-6 small-12 columns matchHeight<?php echo $i % 2 != 0 ? ' negative-margin-top' : '' ?> block-builder-col"
                style="background-color: <?php echo $block_color; ?>">
                <?php if ($link): ?>
                    <a href="<?php echo $link ?>"> </a>
                <?php endif; ?>
                    <h2><?php echo $title; ?></h2>
                    <p><?php echo $text; ?></p>

            </div>
            <?php $i++; ?>
        <?php endwhile; ?>
    </div>
<?php endif; ?>


