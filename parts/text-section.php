<?php
/**
 * The partial template for displaying a text section
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 * @author 2manydots
 */
?>
<?php
    $content = get_sub_field('content'); 
    $padding_top = get_sub_field('padding_top');
    $padding_bottom = get_sub_field('padding_bottom');
    $change_background = get_sub_field('change_background');
?>
<section class="text-section <?php echo ($change_background) ? ' bg-g' : ''; ?>">
    <div class="arrow-container">
        <?php new Sprite('arrow-right-icon'); ?>
    </div>
    <div class="row">
        <div class="columns large-8 large-push-2 <?php echo ($padding_top) ? ' padding-top-50' : ''; ?><?php echo ($padding_bottom) ? ' padding-bottom-50' : ''; ?>">
            <?php echo $content; ?>
        </div>
    </div>
</section>