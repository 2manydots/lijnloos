    <?php if($main_color_1 = get_field('main_color_1','options')): ?>
    <style>
        a:hover {
            color: <?php echo $main_color_1;?>;
        }
        a:focus, a:active {
            color: <?php echo $main_color_1;?>
        }
        h1, h2, h3, h4, h5, h6 {
            color: <?php echo $main_color_1;?>;
        }
        .header-section__caption-title p {
            background-color: <?php echo $main_color_1;?>;
        }
        .main-nav .menu-item-has-children .sub-menu li.current_page_ancestor, .main-nav .menu-item-has-children .sub-menu li.current-menu-item, .main-nav .menu-item-has-children .sub-menu li:hover {
            background-color: <?php echo $main_color_1;?>;
        }
        .mobile-nav {
            background-color: <?php echo $main_color_1;?>;
        }
        .universal-button .fa{
            background-color: <?php echo $main_color_1;?>;
        }
        .mobile-nav li {
            background-color: <?php echo $main_color_1;?>;
        }
        .mobile-nav li {
            background-color: <?php echo $main_color_1;?>;
        }
        .universal-button:hover {
            background-color: <?php echo $main_color_1;?>;
        }
        .universal-button:hover .fa {
            background-color: <?php echo $main_color_1;?>;
        }
        .property-prev:active, .property-prev:focus, .property-prev:hover, .property-next:active, .property-next:focus, .property-next:hover, .overview:active, .overview:focus, .overview:hover {
            background-color: <?php echo $main_color_1;?>;
        }
        .slider-controls:before {
            background-color: <?php echo $main_color_1;?>;
        }
        .pagination.current {
            background: <?php echo $main_color_1;?>;
        }
        .w_pagination ul li.current a {
            background: <?php echo $main_color_1;?>;
        }
        .newsletter-section {
            background-color: <?php echo $main_color_1;?>;
        }
        .newsletter-section__form-holder .gform_footer:after {
            background-color: <?php echo $main_color_1;?>;
        }
        .text-section ul li .arrow_right_icon1 {
            fill: <?php echo $main_color_1;?>;
            /*color: <?php echo $main_color_1;?>;
            background-color: <?php echo $main_color_1;?>;*/
        }
        /*.text-section ol li:before {
           color: <?php echo $main_color_1;?>;
        }*/
        .breadcrumbs .fa {
            color: <?php echo $main_color_1;?>;
        }
        footer {
            background-color: <?php echo $main_color_1;?>;
        }
        .quote-section__wrapper {
            border-top: 1px solid <?php echo $main_color_1;?>;
            border-bottom: 1px solid <?php echo $main_color_1;?>;
        }
        .text-subtext__right ul li .check_icon1 {
            fill:<?php echo $main_color_1;?>;
            /*color: <?php echo $main_color_1;?>;*/
        }
        .text-subtext__right ol li:before {

        }
        .advertisement__wrapper {
            background-color: <?php echo $main_color_1;?>;
        }
        .projectleden__item-phone a{
            color: <?php echo $main_color_1;?>;
        }
        .projectleden__item-phone .phone_icon1 {
            fill: <?php echo $main_color_1;?>;
        }
        .mobile-nav {
            background-color: <?php echo $main_color_1;?>;
        }
        .mobile-nav li {
            background-color: <?php echo $main_color_1;?>;
        }
        .menu-controls__open, .menu-controls__close{
            background-color: <?php echo $main_color_1;?>;
        }
        .image-text .arrow_right_icon1 {
            fill: <?php echo $main_color_1;?>;
        }
        .agenda-container .title {
            color: <?php echo $main_color_1;?>;
        }
        .agenda-container .arrow_right_icon1 {
            fill: <?php echo $main_color_1;?>;
        }
        .titel-links .titel-links__text .arrow_right_icon1 {
            fill: <?php echo $main_color_1;?>;
        }
        .text-subtext .text-subtext__left ul li .arrow_right_icon1 {
            fill: <?php echo $main_color_1;?>;
        }
        .ext-form button[type="submit"]:after {
            background-color: <?php echo $main_color_1;?>;
        }
    </style>
    <?php endif; ?>
    <?php if($main_color_2 = get_field('main_color_2','options')): ?>
        <style>
            .ext-form button[type="submit"]{
                background-color: <?php echo $main_color_2;?>;
            }
            a {
                color: <?php echo $main_color_2;?>;
            }
            .main-nav li a:hover, .main-nav li a:focus {
                color:<?php echo $main_color_2;?>;
            }

            .main-nav .current-menu-item a {
                color: <?php echo $main_color_2;?>;
            }
            .main-nav li:hover a {
                color: <?php echo $main_color_2;?>;
            }
            .main-nav .menu-item-has-children .sub-menu li {
                background-color: <?php echo $main_color_2;?>;
            }
            .menu-controls__close:hover, .menu-controls__open:hover {
                background-color: <?php echo $main_color_2;?>;
            }
            .universal-button {
                background-color: <?php echo $main_color_2;?>;
            }
            .property-prev, .property-next, .overview {
                background-color: <?php echo $main_color_2;?>;
            }
            .w_pagination ul li a {
                background: <?php echo $main_color_2;?>;
            }
            .projects__image-holder {
                border-bottom: 6px solid <?php echo $main_color_2;?>;
            }
            .newsletter-section__form-holder .button, .newsletter-section__form-holder input[type="submit"], .newsletter-section__form-holder button, .newsletter-section__form-holder input[type="submit"], .newsletter-section__form-holder button {
                background-color: <?php echo $main_color_2;?>;
            }
            .breadcrumbs span a span {
                color: <?php echo $main_color_2;?>;
            }
            .downloads__list li .doc_icon_blue1,
            .downloads__list li .glob_svg0 {
                fill: <?php echo $main_color_2;?>;
            }
            .projectleden__item-email a {
                color: <?php echo $main_color_2;?>;
            }

            .projectleden__item-email .email_icon1 {
                fill:  <?php echo $main_color_2;?>;
            }
            a {
                color: <?php echo $main_color_2;?>;
            }
            .main-nav li a:hover, .main-nav li a:focus {
                color: <?php echo $main_color_2;?>;
            }
            .main-nav .current-menu-item a {
                color: <?php echo $main_color_2;?>;
            }
            .main-nav li:hover a {
                color: <?php echo $main_color_2;?>;
            }
            .main-nav .menu-item-has-children .sub-menu li {
                background-color: <?php echo $main_color_2;?>;
            }
            .menu-controls__close:hover, .menu-controls__open:hover {
                background-color: <?php echo $main_color_2;?>;
            }
            .universal-button {
                background-color: <?php echo $main_color_2;?>;
            }
            .property-prev, .property-next, .overview {
                background-color: <?php echo $main_color_2;?>;
            }
            .w_pagination ul li a {
                background: <?php echo $main_color_2;?>;
            }
            .projects__image-holder {
                border-bottom: 6px solid <?php echo $main_color_2;?>;
            }
            .projects__title-single {
                color: <?php echo $main_color_2;?>;
            }
            .newsletter-section__form-holder .button, .newsletter-section__form-holder input[type="submit"], .newsletter-section__form-holder button, .newsletter-section__form-holder input[type="submit"], .newsletter-section__form-holder button {
                background-color: <?php echo $main_color_2;?>;
            }
            .breadcrumbs span a span {
                color: <?php echo $main_color_2;?>;
            }
            /*.projectleden__item-email {
                color: <?php echo $main_color_2;?>;
            }*/

            /*.projectleden__item-email:before {
                color: <?php echo $main_color_2;?>;
            }*/
            .sub-menu > .current-menu-item{
                background-color: <?php echo $main_color_2;?>;
            }
            .main-nav .menu-item-has-children .sub-menu:before {
                border-bottom-color : <?php echo $main_color_2;?>;
            }
            /*.text-subtext__right ol li:before{
               background-color: <?php echo $main_color_2;?>;
            }*/
            /*.advertisement__phone:before{
                background-color: <?php echo $main_color_2;?>;
            }*/
            /*.projectleden__item-phone:before{
                background-color: <?php echo $main_color_1;?>;
            }*/
            .advertisement__email:before{
                background-color: <?php echo $main_color_2;?>;
            }
            /*.projectleden__item-email:before{
                 background-color: <?php echo $main_color_2;?>;
            }*/
            /*.downloads__list li:before{
                background-color: <?php echo $main_color_2;?>;
            }*/
            .projects__internal-link .arrow_right_icon1 {
                fill: <?php echo $main_color_2;?>;
            }
            /*.breadcrumbs .fa-long-arrow-right:before{
                background-color: <?php echo $main_color_1;?>;
            }*/
            .newsletter-section__form-holder .gform_footer:hover input[type="submit"]{
                background-color: <?php echo $main_color_2;?>;
            }
            /*.text-subtext__right ul li:before {
                background-color: <?php echo $main_color_1;?>;
            }*/
            .text-subtext__right ol li:before {

            }
            .mobile-nav .current_page_item, .mobile-nav li:hover{
                background-color: <?php echo $main_color_2;?>;
            }
            .main-nav>.current_page_parent a{
                color: <?php echo $main_color_2;?>;
            }
            .agenda-container .item-link:hover .title{
                color: <?php echo $main_color_2;?>;
            }
            .agenda-container .item-link:hover .arrow_right_icon1 {
                fill: <?php echo $main_color_2;?>;
            }
            .gform_wrapper input[type='submit']{
                background-color: <?php echo $main_color_2;?>;
            }
            .w_pagination ul li .current {
                border:1px solid <?php echo $main_color_2;?>;
                color: <?php echo $main_color_2;?>;
                line-height: 30px;
            }
            
            .new-projects .projects__image-holder {
                border-bottom-color: <?php echo $main_color_1; ?>;
            }
            
            .new-projects .projects__title-single {
                color: <?php echo $main_color_1; ?>;
            }
            
            .new-projects .projects__title-single:hover {
                color: <?php echo $main_color_2; ?>;
            }
            
            .sticky-banner-container .banner {
                background-color: <?php echo $main_color_1; ?>;
            }
            
            .news .projects__title {
                color: <?php echo $main_color_2; ?>;
            }
        </style>
    <?php endif; ?>
