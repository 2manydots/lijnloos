<?php
/**
 * The left sidebar containing the left widget area
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since Grunt Boilerplate 0.1.0
 * @author 2manydots
 */

if ( is_active_sidebar( 'sidebar-left' ) ) :
    dynamic_sidebar( 'sidebar-left' );
endif; ?>