<?php
/**
 * The template for pages with sidebar left
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 */

/* Template name: Sidebar left */

get_header(); ?>

    <main class="main">

        <?php get_template_part('parts/breadcrumbs'); ?>

        <div class="row">
            <div class="medium-8 medium-push-4 columns">

                <?php if ( have_posts() ) : ?>

                    <?php while ( have_posts() ) : the_post(); ?>

                        <div class="row">
                            <div class="medium-12 columns">
                                <h1><?php the_title(); ?></h1>
                                <?php the_content(); ?>
                            </div>
                        </div>

                    <?php endwhile;

                    get_template_part('parts/pagination');

                else :

                    get_template_part('parts/no-content');

                endif; ?>

            </div>
            <div class="medium-4 medium-pull-8 columns">
                <?php get_sidebar('left'); ?>
            </div>
        </div>
    </main>

<?php get_footer(); ?>