<?php
/**
 * The template for displaying archive agenda pages
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 */
/* Template name: Agenda Archive */

get_header(); ?>
    <main class="main">

        <?php get_template_part('parts/breadcrumbs'); ?>
        <div class="row">
            <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                <h1><?php the_title(); ?></h1>
            </div>                
        </div>
        <section class="agenda-container">
            <?php 
                $args = array(
                    'post_type' => 'agenda_pt',
                    'posts_per_page' => -1,
                    'order' => 'ASC',
                    'orderby' => 'meta_value',
                    'meta_query' => array(
                        array(
                            'key' => 'date'
                        )
                    )
                );
                $query = new WP_Query($args);
            ?>
            <?php if ( $query->have_posts() ): ?>
                <div class="row">
                    <div class="large-8 large-push-2 medium-10 medium-push-1 small-12 columns">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>                        
                        <a href="<?php the_permalink(); ?>" class="item-link">
                        <div class="single-item">
                            <div class="title-area">
                                <h4 class="title"><?php the_title(); ?><?php new Sprite('arrow-right-icon'); ?></h4>
                            </div>
                            <?php
                                $date       = get_field('date');
                                $date_end   = get_field('date_end');
                                $start_time = get_field('start_time');
                                $end_time   = get_field('end_time');
                                $type       = get_field_object('type');
                                $type_value = $type['value'];
                                $type_label = $type['choices'][ $type_value ];
                            ?>
                            <?php if( $date ): ?>
                                <div class="meta-row">
                                    <div class="date-row">
                                        <span class="dmy text">
                                            <?php echo $date; ?>
                                            <?php if( $date_end ): ?>
                                                - <?php echo $date_end; ?>
                                            <?php endif; ?>
                                        </span>
                                        <?php if( $start_time && $end_time ): ?>                                                
                                            <span class="start-time text">| <?php echo $start_time; ?></span>
                                            <span class="end-time text">- <?php echo $end_time; ?></span>
                                            <span class="time-text text">uur</span>
                                        <?php endif; ?>
                                    </div>
                                    <?php if( $type_label ): ?>
                                        <div class="type-row">
                                            <span class="type text">
                                                <?php echo $type_label; ?>
                                            </span>                                                
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            
                        </div>
                        </a>
                    <?php endwhile; ?>
                    </div>
                </div>            
            <?php endif; ?>            
        </section>
    </main>

<?php get_footer(); ?>