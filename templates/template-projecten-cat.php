<?php
/* Template name: Project Category */

get_header();
?>

<main class="main">

        <?php get_template_part('parts/breadcrumbs'); ?>

        <section class="projects projects-all exact-project">
            <div class="row">
                <div class="small-12 medium-12 large-12 columns text-left">
                    <h1 class="projects__title"><?php the_title(); ?></h1>
                </div>
            </div>
            <?php
                $project_category = get_field('project_category');
                $args = array(
                    'post_type' => 'projecten',
                    'posts_per_page' => -1,
                    'tax_query' => array(
                            array(
                                    'taxonomy' => 'project_categories',
                                    'field'    => 'term_id',
                                    'terms'    => $project_category,
                            ),
                    ),
                );
                $project_query = new WP_Query($args);
            ?>
            <?php if( $project_query->have_posts() ): ?>
                <div class="row">
                    <?php while ( $project_query->have_posts() ): $project_query->the_post(); ?>
                        <?php
                            $project_link = get_permalink(get_the_ID());
                            $item_thumbnail = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
                            $project_item_title = get_the_title(get_the_ID());
                            $event_categories = get_the_terms( $post, 'project_categories' );
                        ?>
                        <div class="large-4 medium-6 small-12 columns project-col matchHeight">
                            <a href="<?php echo $project_link ?>" class="projects__image-holder">
                                <div class="projects__image" style="background-image: url('<?php echo $item_thumbnail; ?>')"></div>
                            </a>
                            <a class="projects__title-single" href="<?php echo $project_link ?>"><?php echo $project_item_title ?></a>

                            <p class="cat-name">
                            <?php  if ( $event_categories ):
                                foreach($event_categories as $event_cat):
                                    echo $event_cat->name;
                                endforeach;
                            endif; ?>
                            </p>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="row">
                    <?php get_template_part('parts/pagination'); ?>
                </div>
            <?php endif; wp_reset_query(); ?>
        </section>
    </main>
<?php
get_footer();

