<footer>
    <div class="row">
        <div class="large-6 medium-12 columns text-center large-only-text-left ">
            <div class="tmd">
                <?php $copyright = get_field('copyright','options');?>
                &copy; <?= date('Y') ?> <?php echo $copyright;?>
                <?php if( have_rows('page_links','options') ): ?>
                  <?php while( have_rows('page_links','options') ): the_row();
                    $link = get_sub_field('link');
                     ?>
                    <a href="<?php echo $link['url']; ?>" class="footer-page-link"><?php echo $link['text']; ?></a>
                  <?php endwhile; ?>
                <?php endif; ?>
            </div>      
        </div>
        <div class="large-6 medium-12 columns text-center large-only-text-right">
            <p class="tmd">
                Realisatie door <a href="https://www.2manydots.nl/">2manydots</a> en <a href="http://www.rosrobuust.nl">Robuust</a>
            </p>
        </div>
    </div>
</footer>
<?php $sticky_banner = get_field( 'sticky_banner', 'option' ); ?>
<?php if ( $sticky_banner ) : ?>
    <div class="sticky-banner-container closed">
        <div class="row">
            <div class="large-12 medium-12 small-12 columns">
                <div class="banner">
                    <?php echo $sticky_banner; ?>
                    <a href="#" class="close-btn"></a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php get_template_part('parts/body-end'); ?>
<?php wp_footer(); ?>
</body>
</html>