<?php

function tmd_includes() {
    // CSS
    wp_enqueue_style( 'slick', get_template_directory_uri().'/css/plugins/slick.css', null, null );

    wp_enqueue_style("style.css", get_stylesheet_uri(), [], '1.0.1' );
    
    wp_enqueue_style( 'icomoon', get_template_directory_uri() . '/fonts/icomoon/style.css' );
    //plugins
    // JavaScript
    wp_enqueue_script("jquery");
    wp_enqueue_script("main.min.js", get_template_directory_uri() . "/js/main.min.js", array(), "1.0.1", true);
    wp_enqueue_script("slick", get_template_directory_uri() . "/js/plugins/slick.min.js", array(), "1.0.0", true);
    wp_enqueue_script("jquery.matchHeight-min", get_template_directory_uri() . "/js/lib/jquery.matchHeight-min", array(), "1.0.0", true);
}
add_action("wp_enqueue_scripts", "tmd_includes");

?>