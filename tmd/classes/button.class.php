<?php

class Button
{
    private $button;
    private $css_class;

    public function __construct($button, $css_class = '')
    {
        if(!$css_class) {
            $css_class = 'btn';
        }
        $this->initButton($button, $css_class);
    }

    /**
     * Init Button
     *
     * @param $button
     * @param $css_class
     */
    private function initButton($button, $css_class) {
        $this->button = $button;
        $this->css_class = $css_class;
        $button_title = $this->button['button_title'];
        $button_type_of_link = $this->button['button_type'];
        $button_external_link = $this->button['button_external_link'];
        $button_anchor_link = $this->button['button_anchor_link'];
        $button_page_link = $this->button['button_page_link'];
        $button_file_link = $this->button['button_file_link'];
        $button_email_link = $this->button['button_email_link'];
        $button_phone_link = $this->button['button_phone_link'];

        if($button_type_of_link == 'external' && $button_title) ?>
            <a class="<?= $this->css_class;?>" target="_blank" href="<?= $button_external_link;?>"><?= $button_title;?></a>

        <?php if ($button_type_of_link == 'anchor' && $button_title) ?>
            <a class="<?= $this->css_class;?>" href="<?= $button_anchor_link;?>"><?= $button_title;?></a>

        <?php if ($button_type_of_link == 'page' && $button_title) ?>
            <a class="<?= $this->css_class;?>" href="<?= $button_page_link;?>"><?= $button_title;?></a>

        <?php if ($button_type_of_link == 'file' && $button_title) ?>
            <a class="<?= $this->css_class;?>" target="_blank" href="<?= $button_file_link;?>"><?= $button_title;?></a>

        <?php if ($button_type_of_link == 'email' && $button_title) ?>
            <a class="<?= $this->css_class;?>" href="mailto:<?= $button_email_link;?>"><?= $button_title;?></a>

        <?php if ($button_type_of_link == 'phone' && $button_title) ?>
            <a class="<?= $this->css_class;?>" href="tel:<?= $button_phone_link;?>"><?= $button_title;?></a>

    <?php }
}
?>