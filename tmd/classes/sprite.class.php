<?php

class Sprite {
    private $svg_clean_attributes = ["id", "enable-background", "version", "x", "y"];

    /**
     * Creates an inline SVG sprite with PNG fallback for older browsers
     * 
     * @param string        $sprite     Name of the sprite, equals the filename in images/sprites/
     * @param string|int    $width      Width of the sprite
     * @param string|int    $height     Height of the sprite
     * @param boolean       $clean      Whether to clean the SVG from unnecessary attributes (provided by $svg_clean_attributes)
     */
    public function __construct($sprite, $width = false, $height = false, $clean = true) {
        $svg = phpQuery::newDocumentFileXHTML(get_template_directory() . "/images/sprites/" . $sprite . ".svg")->find('svg');

        // Clean the SVG
        if ( $clean ) {
            foreach ( $this->svg_clean_attributes as $attribute ) {
                $svg->removeAttr($attribute);
            }

            // Round width and height
            $svg->attr("width", round((int)$svg->attr("width")));
            $svg->attr("height", round((int)$svg->attr("height")));
        }

        // Resizing
        if ( $width ) $svg->attr("width", $width);
        if ( $height ) $svg->attr("height", $height);

        // Add classes to the SVG element
        // .svg-sprite-inline is used in mixins/_sprite.scss to show/hide the correct element in every browser
        $svg->addClass("svg-sprite-inline svg-sprite-inline-" . $sprite);

        // Echo SVG and fallback
        echo $svg;
        echo '<div class="sprite-' . $sprite . ' svg-sprite-inline-fallback"></div>';
    }
}

?>