<?php

class RetinaImage
{
    private $image;
    private $width;
    private $height;
    private $src;
    private $srcset;

    public function __construct($image, $sizes)
    {
        $this->initImage($image, $sizes);
    }

    /**
     * Init Image
     *
     * @param $image
     * @param $sizes
     */
    private function initImage($image, $sizes) {
        $this->image = $image;
        $this->width = $this->image['sizes'][$sizes[0]['size']  . '-width'];
        $this->height = $this->image['sizes'][$sizes[0]['size']  . '-height'];
        $this->src = '';
        $this->srcset = '';

        $i = 0;
        foreach($sizes as $size) {
            $i++;

            if($i == 1) {
                $this->src = $this->image['sizes'][$size['size']];
            }

            $this->srcset .= $this->image['sizes'][$size['size']] . ' ' .  $size['css'] . ', ';
        }
    }

    /**
     * SetDimensions
     *
     * @param $sizeSlug
     * @param int $decrease
     */
    public function setDimensions($sizeSlug, $decrease = 1) {
        $this->width = $this->image['sizes'][$sizeSlug . '-width'] / $decrease;
        $this->height =$this->image['sizes'][$sizeSlug . '-height'] / $decrease;
    }

    /**
     * PrintImage
     */
    public function printImage() {
        ?>

        <img width="<?= $this->width ?>" height="<?= $this->height ?>" class="image_element"
             src="<?= $this->src ?>"
             srcset="<?= $this->srcset ?>"
             alt="<?= $this->image['alt']; ?>">

        <?php
    }
}
?>