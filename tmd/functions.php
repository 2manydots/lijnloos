<?php

// Theme support
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );

// Custom image sizes
add_image_size( 'full_width_image', 1920, 0, false );
add_image_size( 'full_width_image_2', 1920, 0, false );
add_image_size( 'og_img', 1200, 630, array( 'center', 'center' ));
// add_image_size ( string $name, int $width, int $height, bool|array $crop = false )

// Navigations
function tmd_register_nav_menus() {

    $locations = array(
        'top-nav' => 'Top navigation',
        'main-nav' => 'Main navigation',
        'footer-nav' => 'Footer navigation',
        'mobile-nav' => 'Mobile navigation',
    );
    register_nav_menus( $locations );

}
add_action( 'init', 'tmd_register_nav_menus' );

// Sidebars
function tmd_register_sidebars() {

    $args = array(
        'id'            => 'sidebar-left',
        'name'          => 'Sidebar left',
        'description'   => 'Description',
    );
    register_sidebar( $args );

    $args = array(
        'id'            => 'sidebar-right',
        'name'          => 'Sidebar right',
        'description'   => 'Description',
    );
    register_sidebar( $args );

}
add_action( 'widgets_init', 'tmd_register_sidebars' );

// clear dashboard
function remove_menus() {
  remove_menu_page('edit-comments.php');
//  remove_menu_page('edit.php');
}
add_action('admin_menu', 'remove_menus');


// Create pagination
if (!function_exists('foundation_pagination')) :
    function foundation_pagination($the_query = false) {
        if($the_query) {
            $wp_query = $the_query;
        } else {
            global $wp_query;
        }

        $big = 999999999; // This needs to be an unlikely integer

        // For more options and info view the docs for paginate_links()
        // http://codex.wordpress.org/Function_Reference/paginate_links
        $paginate_links = paginate_links( array(
            'base' => str_replace( $big, '%#%', html_entity_decode( get_pagenum_link( $big ) ) ),
            'current' => max( 1, get_query_var( 'paged' ) ),
            'total' => $wp_query->max_num_pages,
            'mid_size' => 5,
            'prev_next' => true,
            'prev_text' => __( '&laquo;', 'foundationpress' ),
            'next_text' => __( '&raquo;', 'foundationpress' ),
            'type' => 'list',
        ) );

        $paginate_links = str_replace( "<ul class='page-numbers'>", "<ul class='pagination'>", $paginate_links );
        $paginate_links = str_replace( '<li><span class="page-numbers dots">', "<li><a href='#'>", $paginate_links );
        $paginate_links = str_replace( "<li><span class='page-numbers current'>", "<li class='current'><a href='#'>", $paginate_links );
        $paginate_links = str_replace( '</span>', '</a>', $paginate_links );
        $paginate_links = str_replace( "<li><a href='#'>&hellip;</a></li>", "<li><span class='dots'>&hellip;</span></li>", $paginate_links );
        $paginate_links = preg_replace( '/\s*page-numbers/', '', $paginate_links );

        // Display the pagination if more than one page is found.
        if ( $paginate_links ) {
            echo '<div class="pagination-centered">';
            echo $paginate_links;
            echo '</div>';
        }
    }

endif;

// clear wp admin bar
function remove_wp_nodes() {
  global $wp_admin_bar;
//  $wp_admin_bar->remove_node('new-post');
  $wp_admin_bar->remove_node('comments');
}
add_action('admin_bar_menu', 'remove_wp_nodes', 999);

// Theme Settings page
if (function_exists('acf_add_options_page')) {

  acf_add_options_page(array(
      'page_title' => 'Theme Settings',
      'menu_title' => 'Theme Settings',
      'menu_slug' => 'theme-settings',
      'capability' => 'edit_posts',
      'redirect' => false
  ));
}

// button shortcode
// [button link=""]text[/button]
function button_func($atts, $content = null) {


  $attr = shortcode_atts(array(
      'link' => NULL,
          ), $atts);

  $source = $attr['link'];
  $source = '<a href="' . $attr['link'] . '" class="btn"><span>' . $content . '</span></a>';
  return $source;
}
add_shortcode('button', 'button_func');

// custom excerpt
function custom_length_excerpt($word_count_limit) {
    $content = wp_strip_all_tags(get_the_content() , true );
    echo wp_trim_words($content, $word_count_limit);
}

//wp_enqueue_style( 'style2', get_template_directory_uri() . '/style2.css', null, null );/*1st priority*/

add_action('admin_menu','remove_default_post_type');

function remove_default_post_type() {
	remove_menu_page('edit.php');
}
function remove_editor_menu() {
  remove_action('admin_menu', '_add_themes_utility_last', 101);
}
add_action('_admin_menu', 'remove_editor_menu', 1);
//Delete 
function delete_exp_agenda () {
    $today = date( "Y-m-d" );
    $args = array (
        'post_type' => 'agenda_pt',
        'meta_query' => array(
                'relation' => 'AND',
		array(
			'key'     => 'date',
			'value'   => $today,
                        'type'    => 'date',
			'compare' => '<',
		),                
	)        
    );
    $query = new WP_Query($args);
    if( $query->have_posts() ){
        while ( $query->have_posts() ){
            $query->the_post();
            $id = get_the_ID();
            wp_trash_post($id);
        }
    }
  wp_reset_postdata();
}
add_action( 'init', 'delete_exp_agenda' );
add_action( 'untrash_post', 'action_function_name_11' );
function action_function_name_11( $post_id ){
    if( get_post_type($post_id) == 'agenda_pt' ){
        update_post_meta( $post_id, 'date', date( "Y-m-d" ) );
    }
}
add_filter( 'wpseo_og_image', '__return_false' );

//Custom posts per page on Archive Page
function wpd_archive_query( $query ){
    if ( ! is_admin() && ($query->is_post_type_archive('news')) && $query->is_main_query() ) {
        $query->set( 'posts_per_page', 9 );
    }
    
    if ( ! is_admin() && ($query->is_post_type_archive('projecten')) && $query->is_main_query() ){
        $meta_query = [
            'relation' => 'OR',
            [
                'key'     => 'hide_on_archive',
                'value'   => 1,
                'compare' => '!='
            ],
            [
                'key'     => 'hide_on_archive',
                'compare' => 'NOT EXISTS'
            ]
        ];
        
        $query->set( 'meta_query', $meta_query );
    }
}
add_action( 'pre_get_posts', 'wpd_archive_query' );