<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' type='text/css' href='<?php echo get_template_directory_uri(); ?>/sass/style.php'/>
    <link
        href="https://fonts.googleapis.com/css?family=Bree+Serif|Overpass:100,100i,200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <title><?php if (!is_front_page()) {
            wp_title('');
            echo " - ";
        }
        bloginfo('name'); ?></title>

    <?php wp_head(); ?>
    <?php include_once "custom_style.php"; ?>
    
    <?php if( $background_image = get_field('background_image') ): ?>
        <meta property="og:image" content="<?php echo $background_image['sizes']['og_img'];?>" />
    <?php else: ?>
        <?php $header_image = get_field('header_image', 'option'); ?>
        <meta property="og:image" content="<?php echo $header_image['sizes']['og_img'];?>" />        
    <?php endif; ?>
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <?php get_template_part('parts/head-end'); ?>
</head>
<body <?php body_class(); ?>>
<?php get_template_part('parts/body-start'); ?>
<?php
$header_class='';
if (is_post_type_archive('projecten') && !is_tax( 'project_categories' ) ) {    
    ?>
    <?php $hide_header_image = get_field('hide_header_image_proj', 'options'); ?>
    <?php if ($hide_header_image == 'true') {
        if ($background_image = get_field('background_image_proj', 'options')) {
            $background_image = get_field('background_image_proj', 'options');
        } else {
            $background_image = get_field('header_image', 'options');
        };
        $page_title = get_field('page_title_proj', 'options');
    }else{
        $header_class='no-header';
    } ?>
<?php }
elseif( is_tax( 'project_categories' ) ) {
    $header_class='no-header';    
}
elseif (is_post_type_archive('news')) {
    ?>
    <?php $hide_header_image = get_field('hide_header_image_news', 'options'); ?>
    <?php if ($hide_header_image == 'true') {
        if ($background_image = get_field('background_image_news', 'options')) {
            $background_image = get_field('background_image_news', 'options');
        } else {
            $background_image = get_field('header_image', 'options');
        };
        $page_title = get_field('page_title_news', 'options');
    }else{
        $header_class='no-header';
    } ?>
<?php }elseif( is_404() ){ ?>
    <?php if( $select_404_page = get_field('select_404_page', 'option') ): $post = $select_404_page; setup_postdata($post); ?>
        <?php $hide_header_image = get_field('hide_header_image'); ?>
        <?php if ($hide_header_image == 'true') {
            if ($background_image = get_field('background_image')) {
                $background_image = get_field('background_image');
            } else {
                $background_image = get_field('header_image', 'options');
            };
            $page_title = get_field('page_title');
        }else{
            $header_class='no-header';
        }?>
    <?php wp_reset_postdata(); endif; ?>
<?php }else{ ?>
    <?php $hide_header_image = get_field('hide_header_image'); ?>
    <?php if ($hide_header_image == 'true') {
        if ($background_image = get_field('background_image')) {
            $background_image = get_field('background_image');
        } else {
            $background_image = get_field('header_image', 'options');
        };
        $page_title = get_field('page_title');
    }else{
        $header_class='no-header';
    }
} ?>
<section class="header-section <?php echo $header_class;?>"
         style="background-image: url('<?php echo $background_image['sizes']['full_width_image'] ?? ''; ?>')">
    <header class="header sticky">
        <div class="row small-collapse">
            <div class="large-2 small-12 columns text-center small-text-left ">
                <?php if ($logo = get_field('logo', 'options')): ?>
                    <div class="logo">
                        <a href="<?= home_url(); ?>" rel="nofollow"><img src="<?php echo $logo; ?>" alt="logo"></a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="large-10 small-12 columns text-center large-only-text-right show-for-medium-up">
                <?php wp_nav_menu(array('menu_class' => 'main-nav ', 'theme_location' => 'main-nav')) ?>
            </div>
            <div class="columns text-right hide-for-medium-up">
                <div class="mobile-navigation">
                    <div class="menu-controls">
                        <div class="menu-controls__open">
                            <?php new Sprite('hamburger'); ?>
                        </div>
                        <div class="menu-controls__close">
                            <?php new Sprite('close'); ?>
                        </div>
                    </div>
                    <?php wp_nav_menu(array('menu_class' => 'mobile-nav', 'theme_location' => 'mobile-nav', 'container_class' => 'menu-mobile_menu-container' )) ?>
                </div>
            </div>
        </div>
    </header>
    <div class="header-caption-row">
        <div class="row ">
            <div class="large-6 columns ">
                <div class="header-section__caption">
                    <?php if ($page_title = get_field('page_title')): ?>
                        <div class="header-section__caption-title"><?php echo $page_title; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

