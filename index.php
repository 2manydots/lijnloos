<?php
/**
 * The template for displaying the index file
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 */

get_header(); ?>

    <main class="main">

        <?php get_template_part('parts/breadcrumbs'); ?>
        <div class="row">
            <div class="medium-12 columns">

                <?php if ( have_posts() ) : ?>

                    <?php while ( have_posts() ) : the_post(); ?>

                        <div class="row">
                            <div class="medium-12 columns">
                                <h2><?php the_title(); ?></h2>
                                <p><?php custom_length_excerpt(54);  ?></p>
                                <a class="read-more" href="<?= get_permalink( get_the_ID() )?>" title="<?= get_the_title(); ?>"><?= __( 'lees meer', 'tmd-wp-grunt' ) ?></a>
                            </div>
                        </div>

                    <?php endwhile;?>

                    <div class="pag"><?php foundation_pagination(); ?></div>

                <?php else :

                    get_template_part('parts/no-content');

                endif; ?>

            </div>
        </div>
    </main>

<?php get_footer(); ?>